package AoC.day02;

import java.util.ArrayList;

public class Day2Part2 {
    private ArrayList<String> input;

    public Day2Part2(ArrayList<String> input) {
        this.input = input;
    }

    public int run() {
        int rValue = 0;
        String[] gameSplit;
        String[] roundsSplit;
        String[] ballsSplit;
        String[] showSplit;
        int redMin = 0;
        int blueMin = 0;
        int greenMin = 0;
        int temp;
        for (String s : this.input) {
            gameSplit = s.split(":");
            roundsSplit = gameSplit[1].split(";");
            redMin = 0;
            blueMin = 0;
            greenMin = 0;
            for (String string : roundsSplit) {
                ballsSplit = string.split(",");
                for (String value : ballsSplit) {
                    int i = 0;
                    showSplit = value.split(" ");
                    if (showSplit[0].isEmpty()) {
                        i = 1;
                    }
                    if (showSplit[1 + i].contains("red")) {
                        if(Integer.parseInt(showSplit[i]) > redMin || redMin == 0){
                            redMin = Integer.parseInt(showSplit[i]);
                        }
                    } else if (showSplit[1 + i].contains("blue")) {
                        if(Integer.parseInt(showSplit[i]) > blueMin || blueMin == 0){
                            blueMin = Integer.parseInt(showSplit[i]);
                        }
                    } else {
                        if(Integer.parseInt(showSplit[i]) > greenMin || greenMin == 0){
                            greenMin = Integer.parseInt(showSplit[i]);
                        }
                    }
                }
            }
            temp = redMin * greenMin * blueMin;
            rValue += temp;
        }
        return rValue;
    }
}
