package AoC.day02;

import java.util.ArrayList;

public class Day2Part1 {
    private ArrayList<String> input;

    public Day2Part1(ArrayList<String> input) {
        this.input = input;
    }

    public int run() {
        final int RED = 12;
        final int GREEN = 13;
        final int BLUE = 14;
        int rValue = 0;
        String[] gameSplit;
        String[] roundsSplit;
        String[] ballsSplit;
        String tempString;
        String[] showSplit;
        boolean valid = true;
        for (String s : this.input) {
            valid = true;
            gameSplit = s.split(":");
            roundsSplit = gameSplit[1].split(";");
            for (String string : roundsSplit) {
                if (valid) {
                    ballsSplit = string.split(",");
                    for (String value : ballsSplit) {
                        int i = 0;
                        showSplit = value.split(" ");
                        if (showSplit[0].isEmpty()) {
                            i = 1;
                        }
                        if (showSplit[1 + i].contains("red")) {
                            if (Integer.parseInt(showSplit[i]) > RED) {
                                valid = false;
                                break;
                            }
                        } else if (showSplit[1 + i].contains("blue")) {
                            if (Integer.parseInt(showSplit[i]) > BLUE) {
                                valid = false;
                                break;
                            }
                        } else {
                            if (Integer.parseInt(showSplit[i]) > GREEN) {
                                valid = false;
                                break;
                            }
                        }
                    }
                }
            }
            if (valid) {
                String[] parser = gameSplit[0].split(" ");
                rValue += Integer.parseInt(parser[1]);
            }
        }

        return rValue;
    }
}
