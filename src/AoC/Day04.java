package AoC;

import java.sql.Array;
import java.util.ArrayList;
import java.util.HashMap;

public class Day04 {

    private ArrayList<ArrayList<Integer>> yourCard;
    private ArrayList<ArrayList<Integer>> winningNumbers;

    public Day04(ArrayList<String> input){

        this.yourCard = new ArrayList<>();
        this.winningNumbers = new ArrayList<>();

        String[] inputSplit;
        String[] numbers;

        for(String s: input){
            inputSplit = s.split(":");
            inputSplit = inputSplit[1].split("\\|");
            numbers = inputSplit[0].split(" ");
            ArrayList<Integer> card = new ArrayList<>();
            for(String str: numbers){
                if(!str.isEmpty()) {
                    card.add(Integer.parseInt(str));
                }
            }
            this.yourCard.add(card);
            card = new ArrayList<>();
            numbers = inputSplit[1].split(" ");
            for(String str: numbers){
                if(!str.isEmpty()) {
                    card.add(Integer.parseInt(str));
                }
            }
            this.winningNumbers.add(card);
        }
    }

    public int part01(){
        int rValue = 0;
        for(int i = 0; i < yourCard.size(); i += 1){
            ArrayList<Integer> yourCardLoop = yourCard.get(i);
            ArrayList<Integer> winningLoop = winningNumbers.get(i);
            boolean winner = false;
            int temp = 0;
            for(int j: yourCardLoop){
                for(int k: winningLoop){
                    if(j == k){
                        if(winner){
                            temp *= 2;
                        } else {
                            temp += 1;
                            winner = true;
                        }
                    }
                }
            }
            rValue += temp;
        }
        return rValue;
    }

    public int part02(){
        int rValue = 0;
        int copies = 0;
        int j;
        HashMap<Integer, Integer> numberOfCards = new HashMap<>();

        for(int i = 1; i <= yourCard.size(); i += 1){
            numberOfCards.put(i, 1);
        }

        for(int i = 0; i < yourCard.size(); i += 1){
            ArrayList<Integer> yourCardLoop = yourCard.get(i);
            ArrayList<Integer> winningLoop = winningNumbers.get(i);
            copies = numberOfCards.get(i + 1);
            rValue += copies;
            int cardNum = i + 2;
            boolean flag = false;
            while(copies > 0) {
                int cardNumTemp = cardNum;
                for (int a : yourCardLoop) {
                    for (int b : winningLoop) {
                        if (a == b) {
                            int temp = numberOfCards.get(cardNumTemp);
                            numberOfCards.put(cardNumTemp, (temp + 1));
                            cardNumTemp += 1;
                            if(cardNumTemp > yourCard.size()){
                                flag = true;
                                break;
                            }
                        }
                    }
                    if(flag){
                        break;
                    }
                }
                if(flag){
                    break;
                }
                copies -= 1;
            }
        }

        return rValue;
    }
}
