package AoC.day01;

import java.util.ArrayList;

public class Day1Part1 {
    private ArrayList<String> input;
    private final String numbers = "0123456789";
    public Day1Part1(ArrayList<String> input){
        this.input = input;
    }

    public int run(){
        int rValue = 0;
        String combine;
        for (String s: this.input) {
            char firstNumber = ' ';
            char lastNumber = ' ';
            for(int i = 0; i < s.length(); i++){
                char c = s.charAt(i);
                for(int j = 0; j < this.numbers.length(); j++){
                    if(c == this.numbers.charAt(j)){
                        if(firstNumber == ' '){
                            lastNumber = firstNumber = c;
                            break;
                        } else {
                            lastNumber = c;
                            break;
                        }
                    }
                }
            }
            combine = "" + firstNumber + lastNumber;
            rValue += Integer.parseInt(combine);
        }
        return rValue;
    }
}
