package AoC.day01;

import java.util.ArrayList;
import java.util.HashMap;

public class Day1Part2 {
    private ArrayList<String> input;
    private final String numbers = "0123456789";
    private final String[] numberWords= {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine"};

    private HashMap<String, Character> wordToChar = new HashMap<>();
    public Day1Part2(ArrayList<String> input){
        this.input = input;
        this.hashFiller();
    }

    public int run(){
        int rValue = 0;
        for (String s: this.input) {
            char firstNumber = ' ';
            char lastNumber = ' ';
            boolean found = false;
            String combine = "";
            for(int i = 0; i < s.length(); i++){
                char c = s.charAt(i);
                if(Character.getNumericValue(c) < 10){
                    if(firstNumber == ' '){
                        firstNumber = lastNumber = c;
                    } else {
                        lastNumber = c;
                    }
                } else {
                    for (String numberWord : numberWords) {
                        int count = 0;
                        int temp = i;
                        for (int k = 0; k < numberWord.length(); k++) {
                            if((temp + k) >= s.length()){
                                break;
                            }
                            if(numbers.contains("" + s.charAt(i + k)));
                            if (s.charAt(temp + k) == numberWord.charAt(k)) {
                                count += 1;
                            } else {
                                break;
                            }
                        }
                        if (count == numberWord.length()) {
                            if (firstNumber == ' ') {
                                lastNumber = firstNumber = wordToChar.get(numberWord);
                            } else {
                                lastNumber = wordToChar.get(numberWord);
                            }
                            break;
                        }
                    }
                }
            }
            combine += "" + firstNumber + lastNumber;
            rValue += Integer.parseInt(combine);
        }
        return rValue;
    }

    private void hashFiller(){
        wordToChar.put("one", '1');
        wordToChar.put("two", '2');
        wordToChar.put("three", '3');
        wordToChar.put("four", '4');
        wordToChar.put("five", '5');
        wordToChar.put("six", '6');
        wordToChar.put("seven", '7');
        wordToChar.put("eight", '8');
        wordToChar.put("nine", '9');
    }
}
