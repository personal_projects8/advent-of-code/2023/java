package AoC;

import AoC.day01.Day1Part1;
import AoC.day01.Day1Part2;

import AoC.day02.*;
import AoC.day03.Day03Experiment;
import AoC.day03.Day03Part1;
import AoC.day03.Day03Part2;

import java.util.ArrayList;

public class Driver {

    //Day being done
    private int day;

    //File path
    private String filePath;

    private boolean duckThisItsLate = false;
    private ArrayList<String> input;

    /**
     * Constructor for this class
     *
     * @param day  - Day being done
     * @param test - Test data or actual data
     */
    public Driver(int day, boolean test) {
        this.day = day;
        //If the day is < than 10, preappend 0 to the number, if not, add the number
        if (this.day < 10) {
            this.filePath = "./in/0" + this.day + "/";
        } else {
            this.filePath = "./in/" + this.day + "/";
        }

        //Which file is in use
        if (test) {
            this.filePath += "test.txt";
        } else {
            this.filePath += "data.txt";
        }
    }

    public void start() {
        //Day label
        System.out.printf("Day %d\n", this.day);
        try {
            long startTime, endTime;
            startTime = System.currentTimeMillis();
            FileHandler file = new FileHandler(this.filePath);

            switch (this.day) {
                case 1:
                    input = file.getData();
                    startTime = System.currentTimeMillis();
                    Day1Part1 day1Part1 = new Day1Part1(file.getData());
                    int part1Res = day1Part1.run();
                    endTime = System.currentTimeMillis();
                    System.out.printf("Part 1(%dms).....%d\n", (endTime - startTime), part1Res);
                    startTime = System.currentTimeMillis();
                    Day1Part2 day1Part2 = new Day1Part2(input);
                    int part2Res = day1Part2.run();
                    endTime = System.currentTimeMillis();
                    System.out.printf("Part 2(%dms).....%d\n", (endTime - startTime), part2Res);
                    break;
                case 2:
                    input = file.getData();
                    startTime = System.currentTimeMillis();
                    Day2Part1 d2part1 = new Day2Part1(file.getData());
                    int d2part1Res = d2part1.run();
                    endTime = System.currentTimeMillis();
                    System.out.printf("Part 1(%dms).....%d\n", (endTime - startTime), d2part1Res);
                    startTime = System.currentTimeMillis();
                    Day2Part2 day2Part2 = new Day2Part2(input);
                    int day2part2Res = day2Part2.run();
                    endTime = System.currentTimeMillis();
                    System.out.printf("Part 2(%dms).....%d\n", (endTime - startTime), day2part2Res);
                    break;
                case 3:
                    input = file.getData();
                    startTime = System.currentTimeMillis();
                    Day03Part1 d3part1_1 = new Day03Part1(file.getData());
                    int d3part1_1Res = d3part1_1.run();
                    endTime = System.currentTimeMillis();
                    System.out.printf("Part 1_1(%dms).....%d\n", (endTime - startTime), d3part1_1Res);
                    startTime = System.currentTimeMillis();
                    Day03Experiment d3Experiment = new Day03Experiment(file.getData());
                    int d3part1_2Res = d3Experiment.part1();
                    endTime = System.currentTimeMillis();
                    System.out.printf("Part 1_2(%dms).....%d\n", (endTime - startTime), d3part1_2Res);
                    startTime = System.currentTimeMillis();
                    int d3part2Res = d3Experiment.part2();
                    endTime = System.currentTimeMillis();
                    System.out.printf("Part 2(%dms).....%d\n", (endTime - startTime), d3part2Res);
                    break;
                case 4:
                    input = file.getData();
                    Day04 day04 = new Day04(input);
                    endTime = System.currentTimeMillis();
                    System.out.printf("Generator(%dms)\n", (endTime - startTime));
                    startTime = System.currentTimeMillis();
                    int d4part1Res = day04.part01();
                    endTime = System.currentTimeMillis();
                    System.out.printf("Part 1(%dms).....%d\n", (endTime - startTime), d4part1Res);
                    startTime = System.currentTimeMillis();
                    int d4part2Res = day04.part02();
                    endTime = System.currentTimeMillis();
                    System.out.printf("Part 2(%dms).....%d\n", (endTime - startTime), d4part2Res);
                    break;
                default:
                    System.out.println("Invalid day");
            }
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
