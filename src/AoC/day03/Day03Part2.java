package AoC.day03;

import java.util.ArrayList;

public class Day03Part2 {
    private ArrayList<String> input_unparsed;
    private String[][] input_parsed;

    private String notSymbol = "0123456789.";

    public Day03Part2(ArrayList<String> input) {
        this.input_unparsed = input;
    }

    public int run() {
        int rValue = 0;
        char[][] lineParsed = new char[input_unparsed.size()][input_unparsed.get(0).length()];
        int count = 0;
        for (String s : input_unparsed) {
            for (int i = 0; i < s.length(); i += 1) {
                switch (s.charAt(i)) {
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        lineParsed[count][i] = s.charAt(i);
                        break;
                    case '*':
                        lineParsed[count][i] = 's';
                        break;
                    default:
                        lineParsed[count][i] = '\0';
                }
            }
            count += 1;
        }
        String numbers = "0123456789";
        for(int i = 0; i < lineParsed.length; i +=1){
            for(int j = 0; j < lineParsed[i].length; j += 1){
                if(lineParsed[i][j] == 's'){
                    ArrayList<String> number = new ArrayList<>();
                    for(int a = i - 1; a < i +1; a += 1){
                        if(a < 0){
                            a += 1;
                        }
                        for(int b = j - 1; b < j + 1; b += 1){
                            if(b < 0){
                                b += 1;
                            }
                            if(numbers.contains(Character.toString(lineParsed[a][b]))){
                                int temp = b;
                                String leftNum = "";
                                String rightNum = "";
                                String numberBuilder = "";
                                while(numbers.contains(Character.toString(lineParsed[a][temp]))){
                                    leftNum += lineParsed[a][temp];
                                    temp -= 1;
                                    if(temp < 0){
                                        break;
                                    }
                                }
                                temp = b + 1;
                                while(numbers.contains(Character.toString(lineParsed[a][temp]))){
                                    rightNum += lineParsed[a][temp];
                                    temp += 1;
                                }
                                for(int x = leftNum.length() - 1; x > -1; x -= 1){
                                    numberBuilder += leftNum.charAt(x);
                                }
                                for(int x = 0; x < rightNum.length(); x += 1){
                                    numberBuilder += rightNum.charAt(x);
                                }
                                number.add(numberBuilder);
                            }
                        }
                    }
                }
            }
        }
        return rValue;
    }
}