package AoC.day03;

import java.util.*;

public class Day03Experiment {
    Set<Character> symbols;
    ArrayList<String> input;

    public Day03Experiment(ArrayList<String> input) {
        this.input = input;
        this.symbols = new HashSet<>();
        this.setup();
    }

    public int part1() {
        int rValue = 0;
        String isNumber = "0123456789";
        for (int i = 0; i < input.size(); i += 1) {
            String s = input.get(i);
            int numStart = -1;
            String numFound = "";
            boolean endOfNum = false;
            for (int j = 0; j <= s.length(); j += 1) {
                if (j < s.length()) {
                    if (isNumber.contains(Character.toString(s.charAt(j)))) {
                        if (numStart == -1) {
                            numStart = j;
                        }
                        numFound += s.charAt(j);
                    } else {
                        rValue += part1NumConverter(numFound, i, numStart, j, s);
                        numFound = "";
                        numStart = -1;
                    }
                } else {
                    rValue += part1NumConverter(numFound, i, numStart, j, s);
                    numFound = "";
                    numStart = -1;
                }
            }
        }
        return rValue;
    }

    public int part2() {
        int rValue = 0;
        String numbers = "0123456789";
        HashMap<Integer, HashMap<Integer, ArrayList<Integer>>> locationOfPossibleGears = new HashMap<>();
        boolean numberExists;
        for (int i = 0; i < input.size(); i += 1) {
            String numberFound = "";
            int numberStart = -1;
            String s = input.get(i);
            for (int j = 0; j <= s.length(); j += 1) {
                if (j != s.length()) {
                    if (numbers.contains(Character.toString(s.charAt(j)))) {
                        if (numberStart == -1) {
                            numberStart = j;
                        }
                        numberFound += s.charAt(j);
                    } else {
                        if (numberStart != -1) {
                            for (int a = i - 1; a <= i + 1; a += 1) {
                                if (a < 0) {
                                    a += 1;
                                } else if(a == input.size()){
                                    break;
                                }
                                String str = input.get(a);
                                for (int b = numberStart - 1; b <= j; b += 1) {
                                    if (b == -1) {
                                        b += 1;
                                    } else if(b == str.length()){
                                        break;
                                    }
                                    if (str.charAt(b) == '*') {
                                        HashMap<Integer, ArrayList<Integer>> yCordFromList = locationOfPossibleGears.get(a);
                                        if (yCordFromList == null) {
                                            HashMap<Integer, ArrayList<Integer>> yCord = new HashMap<>();
                                            ArrayList<Integer> number = new ArrayList<>();
                                            yCord.put(b, number);
                                            locationOfPossibleGears.put(a, yCord);
                                        }
                                        ArrayList<Integer> numberList = locationOfPossibleGears.get(a).get(b);
                                        if (numberList == null) {
                                            ArrayList<Integer> number = new ArrayList<>();
                                            locationOfPossibleGears.get(a).put(b, number);
                                            locationOfPossibleGears.put(a, yCordFromList);
                                        }
                                        locationOfPossibleGears.get(a).get(b).add(Integer.parseInt(numberFound));
                                    }
                                }
                            }
                            numberFound = "";
                            numberStart = -1;
                        }
                    }
                } else {
                    if (numberStart != -1) {
                        for (int a = i - 1; a <= i + 1; a += 1) {
                            if (a < 0) {
                                a += 1;
                            } else if(a == input.size()){
                                break;
                            }
                            String str = input.get(a);
                            for (int b = numberStart - 1; b <= j; b += 1) {
                                if (b == -1) {
                                    b += 1;
                                } else if(b == str.length()){
                                    break;
                                }
                                if (str.charAt(b) == '*') {
                                    HashMap<Integer, ArrayList<Integer>> yCordFromList = locationOfPossibleGears.get(a);
                                    if (yCordFromList == null) {
                                        HashMap<Integer, ArrayList<Integer>> yCord = new HashMap<>();
                                        ArrayList<Integer> number = new ArrayList<>();
                                        yCord.put(b, number);
                                        locationOfPossibleGears.put(a, yCord);
                                    }
                                    ArrayList<Integer> numberList = locationOfPossibleGears.get(a).get(b);
                                    if (numberList == null) {
                                        ArrayList<Integer> number = new ArrayList<>();
                                        locationOfPossibleGears.get(a).put(b, number);
                                        locationOfPossibleGears.put(a, yCordFromList);
                                    }
                                    locationOfPossibleGears.get(a).get(b).add(Integer.parseInt(numberFound));
                                }
                            }
                        }
                        numberFound = "";
                        numberStart = -1;
                    }
                }
            }
        }
        for(HashMap<Integer, ArrayList<Integer>> column : locationOfPossibleGears.values()){
            for(ArrayList<Integer> numbersFound: column.values()){
                if(numbersFound.size() == 2){
                    int temp = 1;
                    for(int n : numbersFound){
                        temp *= n;
                    }
                    rValue += temp;
                }
            }
        }
        return rValue;
    }

    private void setup() {
        String notASymbol = "0123456789.";
        for (String s : this.input) {
            for (int i = 0; i < s.length(); i += 1) {
                if (!notASymbol.contains(Character.toString(s.charAt(i)))) {
                    this.symbols.add(s.charAt(i));
                }
            }
        }
    }

    private int part1NumConverter(String numFound, int i, int numStart, int j, String s) {
        if (!numFound.equals("")) {
            boolean count = false;
            for (int a = i - 1; a <= i + 1; a += 1) {
                if (a < 0) {
                    a += 1;
                } else if (a == input.size()) {
                    break;
                }
                String check = this.input.get(a);
                for (int b = numStart - 1; b <= j; b += 1) {
                    if (b < 0) {
                        b += 1;
                    } else if (b == s.length()) {
                        break;
                    }
                    if (this.symbols.contains(check.charAt(b))) {
                        count = true;
                        break;
                    }
                }
            }
            if (count) {
                return Integer.parseInt(numFound);
            }
        }
        return 0;
    }
}
