package AoC.day03;

import java.util.ArrayList;

public class Day03Part1 {
    private ArrayList<String> input_unparsed;
    private String[][] input_parsed;

    private String notSymbol = "0123456789.";

    public Day03Part1(ArrayList<String> input){
        this.input_unparsed = input;
    }

    public int run(){
        int rValue = 0;
        char[][] lineParsed = new char[input_unparsed.size()][input_unparsed.get(0).length()];
        int j = 0;
        for(String s : input_unparsed){
            for(int i = 0; i < s.length(); i += 1){
                switch (s.charAt(i)){
                    case '0':
                    case '1':
                    case '2':
                    case '3':
                    case '4':
                    case '5':
                    case '6':
                    case '7':
                    case '8':
                    case '9':
                        lineParsed[j][i] = s.charAt(i);
                        break;
                    case '.':
                        lineParsed[j][i] = '\0';
                        break;
                    default:
                        lineParsed[j][i] = 's';
                }
            }
            j += 1;
        }
        String numbers = "0123456789";
        boolean count = false;
        String numberCombines = "";
        ArrayList<String> countedNumbers = new ArrayList<>();
        for(int i = 0; i < input_unparsed.size(); i += 1){
            if(count){
                rValue += Integer.parseInt(numberCombines);
                countedNumbers.add(numberCombines);
                count = false;
            }
            numberCombines = "";
            for(j = 0; j < lineParsed[i].length; j += 1){
                if(numbers.contains(lineParsed[i][j] + "")){
                    if(!count) {
                        if (i != 0) {
                            if (j != 0) {
                                if (lineParsed[i - 1][j - 1] == 's') {
                                    count = true;
                                }
                            }
                            if (lineParsed[i - 1][j] == 's') {
                                count = true;
                            }
                            if(j + 1 < lineParsed[i].length) {
                                if (lineParsed[i - 1][j + 1] == 's') {
                                    count = true;
                                }
                            }
                        }
                        if (j != 0) {
                            if (lineParsed[i][j - 1] == 's') {
                                count = true;
                            }
                            if(i + 1 < lineParsed.length){
                                if(lineParsed[i + 1][j - 1] == 's') {
                                    count = true;
                                }
                            }
                        }
                        if(i + 1 < lineParsed.length) {
                            if (lineParsed[i + 1][j] == 's') {
                                count = true;
                            }
                            if(j + 1 < lineParsed[i].length){
                                if(lineParsed[i + 1][j + 1] == 's'){
                                    count = true;
                                }
                            }
                        }
                        if(j + 1 < lineParsed[i].length) {
                            if (lineParsed[i][j + 1] == 's') {
                                count = true;
                            }
                        }
                    }
                    numberCombines += lineParsed[i][j];
                } else {
                    if(count){
                        rValue += Integer.parseInt(numberCombines);
                        countedNumbers.add(numberCombines);
                        count = false;
                    }
                    numberCombines = "";
                }
            }
        }
        if(count){
            rValue += Integer.parseInt(numberCombines);
            countedNumbers.add(numberCombines);
            count = false;
        }
        for(String s: countedNumbers){
            //System.out.println(s);
        }
        return rValue;
    }
}
